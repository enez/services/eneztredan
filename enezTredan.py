#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
|   Ce logiciel "enezTredan" fait partie de la suite "enez" développée par "finizi".
|   Il est mis à disposition selon les termes de la licence Creative Commons Atribution 4.0 International
|   Créé le 04/03/2019
|   my.inizisoft.net/grav
|
|     Code sous licence (CC BY-SA 4.0) :
|      Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International
|
|     This program is free software: you can redistribute it and/or modify
|      it under the terms of the Creative Commons Attribution-ShareAlike 4.0 International 
|      (CC BY-SA 4.0) https://creativecommons.org/licenses/by-sa/4.0/.
|     You are free to:
|        Share : copy and redistribute the material in any medium or format
|        Adapt : remix, transform, and build upon the material
|      for any purpose, even commercially.
|     This license is acceptable for Free Cultural Works.
|     The licensor cannot revoke these freedoms as long as you follow the license terms.
"""

"""
  Script python tournant idéalement sur le server de base de données 'enez'
    pour lire les informations issues du compteur Enedis, fournies sous forme de document json par un Arduino
    et de les publier vers le flux MQTT défini par 'enezKreiz' et enregistrés dans la base de données.
"""

import paho.mqtt.client as mqtt
import simplejson as json
import threading
import serial 
import time
import enezTredanConfig as cfg
import psycopg2 as psql
import random



###########################################################
# Accès à la base de données
#
def db():
  """ Connection à la base de données 
      avec mise en cache dans c_db
  """
  try:
    print('Retreive connection to database')
    c_db = psql.connect(
              host = cfg.DB_PARAMS['host'],
              port = cfg.DB_PARAMS['port'],
              database = cfg.DB_PARAMS['base'],
              user = cfg.DB_PARAMS['user'],
              password = cfg.DB_PARAMS['pwd'] 
              )
  except Exception as ex:
    print("DataBase unreachable")
    print(ex)
  return c_db

def getParam(param):
  """ Lecture d'un paramètre enregistré en base de données
  """
  cur = db().cursor()
  data = {}
  data["param"] = param
  query = "SELECT param_json FROM params WHERE (param_code = %(param)s)"
  try:
    cur.execute(query, data)
    return cur.fetchone()[0]
  except Exception as ex:
    print("Unable to SELECT parameter %s" % param)
    return None

###########################################################
# Les callback MQTT
#
def mqttOnConnect(client, userdata, flags, rc):
  if(rc == 0):
    # Le flag pour controler la connexion
    client.connectedFlag = True
    print("MQTT connected ok")
  else:
    print("MQTT Bad connection Returned code=",rc)

def mqttOndisconnect(client, userdata, rc):
  client.connectedFlag = False


################################################################################

class readFromSerial(threading.Thread):
  """ Lecture du port USB sur lequel est branché la téléInformation.
      La lecture se fait dans un thread différent.
  """

  def __init__(self): 
    self._OK = 0
    self._l = 100
    try:
      threading.Thread.__init__(self)
      self.setDaemon(True)
      self._ser = serial.Serial( port=cfg.SETTINGS['serialPort'], 
                                 baudrate=cfg.SETTINGS['serialSpeed'], 
                                 timeout=1
                                 )
      self._OK = 1
    except:
      print('Serial teleinfo error')
        
  def __del__(self):
    self._ser.close()

  def run(self):
    while True:
      try:
        line = self._ser.readline()
        # Ne traiter que ce qui ressemble à un document json (commençant par '{')
        if (line[0] == ord('{')):
          # Envoyer à mqtt
          try:
            ret = mqttc.publish(topic=cfg.SETTINGS['topic'], payload=line.rstrip(), retain=False)
          except:
            print("MQTT Connection failed")
      except:
        print('Serial no data')	
        continue

################################################################################
# routine principale

if __name__ == '__main__':

  # Récupérer les caractéristiques du server MQTT, enregistrées en base de données
  mqttCfg = getParam('mqtt')
  # Nommer le client MQTT de façon unique
  mqttClientNum = "enezTredan" + str(random.randint(10000, 99999))
  mqtt.Client.connectedFlag = False                #create flag in class
  mqttc = mqtt.Client(client_id = mqttClientNum)   #create new instance 
  mqttc.on_connect = mqttOnConnect                 #bind call back function
  mqttc.on_disconnect = mqttOndisconnect           #bind call back function
  # Initialiser le client MQTT
  mqttc.loop_start()
  # Se connecter au broker
  mqttc.connect(mqttCfg['broker'], int(mqttCfg['port']), 60)
  while not mqttc.connectedFlag:
    print("Wait connect loop")
    time.sleep(1)

  # Instancie & démarre les threads de lecture de la TéléInfo
  tiReader = readFromSerial()
  tiReader.start()

  # Lancer une boucle infinie
  while True:
    time.sleep(15)

  mqttc.loop_stop()    #Stop loop 
  mqttc.disconnect()   # disconnect



