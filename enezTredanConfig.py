#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
  Script python tournant idéalement sur le server de base de données 'enez'
  pour lire les informations issues du compteur Enedis, fournies sous forme de document json par un Arduino
  et de les publier vers le flux MQTT défini par 'enezKreiz' et enregistrés dans la base de données.
  Fichier de configuration.
"""

SETTINGS = { 
  'topic':'test/edf', 
  'serialPort':'/dev/ttyUSB0',
  'serialSpeed':'115200',
  }


DB_PARAMS = { 'host':'localhost', 'port':5432, 'base':'db', 'user':'user', 'pwd':'pwd' }


