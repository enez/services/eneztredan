﻿# enezTredan

Lecture des données du compteur Enedis, par l'intermédiaire d'un Arduino.  
Les données sont ensuite publiées sur le service MQTT "enez" afin que celles-ci soient disponibles, notamment pour que le service "enezDiaz" puisse les enregistrer.  